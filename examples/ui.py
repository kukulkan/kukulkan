import sys
import os


kukulkan_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
py_kukulkan = os.path.join(kukulkan_path, 'python')


sys.path.append(py_kukulkan)

os.chdir(kukulkan_path)

import kukulkan.gui


def main():
    kukulkan.gui.show()


if __name__ == '__main__':
    main()

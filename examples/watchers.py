import sys
import os


kukulkan_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
py_kukulkan = os.path.join(kukulkan_path, 'python')


sys.path.append(py_kukulkan)

os.chdir(kukulkan_path)

import kukulkan.config.watchers as _watchers


def main():
    import time
    watcher = _watchers.FolderWatcher(
        'WatchDog',
        '/home/jongjong/projects/kukulkan/python/kukulkan/config/default',
    )
    watcher.setDaemon(True)
    watcher.start()
    while True:
        try:
            time.sleep(.01)
        except KeyboardInterrupt:
            sys.exit('Stopped execution of config test.')


if __name__ == '__main__':
    main()

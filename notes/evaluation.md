# Evaluation notes

Some nodes propagate the evaluation forward
Some get evaluated when needed

## Automatic evaluation
Math nodes and the likes are automatically evaluated when there input(s)
change(s).

## Evaluation propagation
Flow control nodes will only propagate evaluation under certain conditions.
They will have an evaluation plug to define when they get executed and what gets executed next.

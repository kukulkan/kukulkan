# The Model thingy

We shall have the following components to make the model work:

* a `NodeModel` class, storing run-time data about a specific node.
* a `NodeType` class, storing data for a specific node type.


Examples of node types would be `Add`, `Float`, `ForLoop`, etc...

The `NodeModel` class shall have three attributes (for now):

* a `name` attribute to define the node name in the graph.
* a `parent` attribute to define its parent node.
* a `type` attribute to define the `NodeType` class used for graph evaluation logic.


Additionaly, we should have an `Attribute` class acting like an ORM field to handle getting/setting `NodeModel` data, as well as notifying the event system when data is mutated.

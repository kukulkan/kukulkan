It began with the forging or the great Plugs.  
Three were given to the Logics, wisest... fairest of all nodes.  
Seven to the Operators, great evaluators of the Framed Halls.  
And Nine... nine Plugs were gifted to the race of Numerics who, above all else, desire power.  
For within this Plugs was bound the strength and will to govern each race.  
But they were all of them deceived... For another Plug was made.  
In the land of Graph, in the fires of Mount Group the Dark Lord Root Node forged in secred a Master Plug to control all others...  
And into this Plug he poured a bit of his code, his attributes and his will to connect all nodes.  
One plug to evaluate them all...  

import logging
import traceback

import kukulkan.api.command


LOGGER = logging.getLogger(__name__)


class RunScript(kukulkan.api.command.Command):
    """Run a script in Kukulkan."""

    def run(self):
        script = self.args[0]
        try:
            exec(script, {}, {})
        except:
            err = traceback.format_exc()
            LOGGER.error(err)

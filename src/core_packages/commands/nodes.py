import kukulkan.api.command
import kukulkan.nodes


class CreateNode(kukulkan.api.command.Command):
    """Create a new node."""

    def run(self):
        name, node_type = self.args
        if node_type not in kukulkan.nodes.types:
            raise ValueError('Node type {} does not exist.'.format(node_type))
        self.node = kukulkan.nodes.types[node_type](name)
        return self.node

    def undo(self):
        """Delete the node."""
        self.node.delete()


class DeleteNode(kukulkan.api.command.Command):
    """Delete a node."""

    def __init__(self, model):
        super().__init__()
        self.model = model
        self.node_class = None
        self.node_name = None
        self.node_attributes = {}

    def run(self):
        self.node_class = self.model.__class__
        self.node_name = self.model.name
        self.node_attributes = {}
        for attr in self.model.attributes:
            if attr.socket != 'input':
                continue
            self.node_attributes[attr.name] = attr.value
        self.model.delete()

    def undo(self):
        self.model = self.node_class(self.node_name)
        for attr, value in self.node_attributes.items():
            getattr(self.model, attr).value = value

    def redo(self):
        self.model.delete()

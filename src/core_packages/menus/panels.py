from PyQt5 import QtWidgets
import kukulkan.events
import kukulkan.apps


class PanelsMenu(QtWidgets.QMenu):

    def __init__(self, *args, **kwargs):
        super(PanelsMenu, self).__init__(*args, **kwargs)
        kukulkan.events.subscribe('app.ready', self.populate)

    def populate(self):
        """For each panel in the app, create an action to toogle it."""
        app = kukulkan.apps.current
        for name, panel in app.panels.items():
            action = panel.toggleViewAction()
            action.setText(name)
            self.addAction(action)

"""Base types for kukulkan attributes."""
import kukulkan.api.model.attribute as _attribute_model


class Float(_attribute_model.Attribute):
    """A `float` `Attribute`."""

    default_value = 0.0

    def cast_value(self, value):
        if isinstance(value, str):
            return float(len(value))
        return float(value)


class Integer(_attribute_model.Attribute):
    """An `int` `Attribute`."""

    default_value = 0

    def cast_value(self, value):
        if isinstance(value, str):
            return int(len(value))
        return int(value)


class String(_attribute_model.Attribute):
    """A `str` `Attribute`."""

    default_value = ''

    def cast_value(self, value):
        return str(value)


class Boolean(_attribute_model.Attribute):
    """A `bool` `Attribute`."""

    default_value = False

    def cast_value(self, value):
        return bool(value)


class Enum(_attribute_model.Attribute):
    """An `Attribute` with value choices.

    You should override the ``choices`` attribute in your class or
    instances to allow this attribute to be set.

    Trying to set the value to anything else that what ``choices``
    allows will result in a `ValueError`.
    """

    choices = [None]

    def cast_value(self, value):
        """Ensure the passed ``value`` is contained in ``choices``.

        :raise ValueError: When a forbidden value is passed.
        """
        if value not in self.choices:
            raise ValueError(
                'Value should be one of {}'.format(', '.join(self.choices))
            )
        return value

class Execution(_attribute_model.ExecutionAttribute):
    """An Attribute that propagates execution."""

"""Common operators for values."""
import kukulkan.api.model.node as _node_model
import kukulkan.attributes as _attributes


class Add(_node_model.Node):

    output = _attributes.Float(0, socket='output')
    input1 = _attributes.Float(0, socket='input')
    input2 = _attributes.Float(0, socket='input')

    def run(self):
        out_value = self.input1.value + self.input2.value
        self.output.value = out_value


class Substract(_node_model.Node):

    output = _attributes.Float(0, socket='output')
    input1 = _attributes.Float(0, socket='input')
    input2 = _attributes.Float(0, socket='input')

    def run(self):
        out_value = self.input1.value - self.input2.value
        self.output.value = out_value


class Multiply(_node_model.Node):

    output = _attributes.Float(0, socket='output')
    input1 = _attributes.Float(1, socket='input')
    input2 = _attributes.Float(1, socket='input')

    def run(self):
        out_value = self.input1.value * self.input2.value
        self.output.value = out_value


class Divide(_node_model.Node):
    """Divides to input values."""

    output = _attributes.Float(0, socket='output')
    input1 = _attributes.Float(1, socket='input')
    input2 = _attributes.Float(1, socket='input')

    def run(self):
        if self.input2.value == 0:
            if self.input1.value >= 0:
                out_value = float('+inf')
            else:
                out_value = float('-inf')
        else:
            out_value = self.input1.value / self.input2.value
        self.output.value = out_value

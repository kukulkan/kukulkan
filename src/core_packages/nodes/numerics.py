"""Common math operations."""
import logging
import math

import kukulkan.api.model.node as _node_model
import kukulkan.attributes as _attributes


LOGGER = logging.getLogger(__name__)


class Ceil(_node_model.Node):
    """Compute the ceiling of `input`."""

    input = _attributes.Float(1, socket='input')
    output = _attributes.Integer(0, socket='output')

    def run(self):
        self.output.value = math.ceil(self.input.value)


class CopySign(_node_model.Node):
    """Copy the sign of `input1` to `input2`."""

    input1 = _attributes.Float(1, socket='input')
    input2 = _attributes.Float(0, socket='input')
    output = _attributes.Float(0, socket='output')

    def run(self):
        self.output.value = math.copysign(self.input1.value, self.input2.value)


class Absolute(_node_model.Node):
    """Compute the absolute value of `input`."""

    input = _attributes.Float(1, socket='input')
    output = _attributes.Integer(0, socket='output')

    def run(self):
        self.output.value = math.fabs(self.input.value)


class Factorial(_node_model.Node):
    """Compute the factorial of `input`."""

    input = _attributes.Float(1, socket='input')
    output = _attributes.Float(0, socket='output')

    def run(self):
        self.output.value = math.factorial(self.input.value)


class Floor(_node_model.Node):
    """Compute the floor of `input`."""

    input = _attributes.Float(1, socket='input')
    output = _attributes.Integer(0, socket='output')

    def run(self):
        self.output.value = math.floor(self.input.value)


class Modulo(_node_model.Node):
    """Compute `input1` modulo `input2`."""

    input1 = _attributes.Float(1, socket='input')
    input2 = _attributes.Float(1, socket='input')
    output = _attributes.Float(1, socket='output')

    def run(self):
        value = self.input2.value
        if value == 0:
            value = float('NaN')
            msg = 'Modulo input2 value should be different than 0. '
            msg += 'Resulting to "NaN".'
            LOGGER.warning(msg)
        self.output.value = math.fmod(self.input1.value, value)


class IsInfinite(_node_model.Node):
    """Check if `input` is positive or negative infinity."""

    input = _attributes.Float(0, socket='input')
    output = _attributes.Boolean(0, socket='output')

    def run(self):
        self.output.value = math.isinf(self.input.value)


class IsNan(_node_model.Node):
    """Check if `input` is a `NaN`."""

    input = _attributes.Float(0, socket='input')
    output = _attributes.Boolean(0, socket='output')

    def run(self):
        self.output.value = math.isnan(self.input.value)


class Truncate(_node_model.Node):
    """Compute the truncated value of `input`."""

    input = _attributes.Float(0, socket='input')
    output = _attributes.Integer(0, socket='output')

    def run(self):
        self.output.value = math.trunc(self.input.value)


class Exponential(_node_model.Node):
    """Compute the exponential of `input`."""

    input = _attributes.Float(0, socket='input')
    output = _attributes.Float(0, socket='output')

    def run(self):
        self.output.value = math.exp(self.input.value)


class Logarithmic(_node_model.Node):
    """Compute the logarithmic of `input`."""

    input = _attributes.Float(1, socket='input')
    output = _attributes.Float(0, socket='output')

    def run(self):
        value = self.input.value
        if value == 0:
            value = float('-inf')
            msg = 'Logarithmic input value should be greater than 0. '
            msg += 'Resulting to "-inf".'
            LOGGER.warning(msg)
        elif value < 0:
            value = float('NaN')
            msg = 'Logarithmic input value should be greater than 0. '
            msg += 'Resulting to "NaN".'
            LOGGER.warning(msg)
        else:
            value = math.log(value)
        self.output.value = value


class Power(_node_model.Node):
    """Compute `input` to the power `exponent`."""

    input = _attributes.Float(0, socket='input')
    exponent = _attributes.Integer(0, socket='input')
    output = _attributes.Float(0, socket='output')

    def run(self):
        self.output.value = math.pow(self.input.value, self.exponent.value)


class SquareRoot(_node_model.Node):
    """Compute the square root of `input`."""

    input = _attributes.Float(0, socket='input')
    output = _attributes.Float(0, socket='output')

    def run(self):
        self.output.value = math.sqrt(self.input.value)


# class frexp(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(1, 'input')
#     mantissa = _attributes.Float(0, 'output')
#     exponent = _attributes.Integer(0, 'output')
#
#     def run(self):
#         self.mantissa.value, self.exponent.value = math.frexp(self.input.value)
#
#
# class ldexp(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     exponent = _attributes.Integer(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.ldexp(self.input.value, self.exponent.value)
#
#
# class modf(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     integer = _attributes.Integer(0, 'output')
#     fractional = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.fractional.value, self.integer.value = math.modf(self.input.value)
#
#
# class acos(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.acos(self.input.value)
#
#
# class asin(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.asin(self.input.value)
#
#
# class atan(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.atan(self.input.value)
#
#
# class cos(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.cos(self.input.value)
#
#
# class hypot(_node_model.Node):
#     """"""
#
#     input1 = _attributes.Float(0, 'input')
#     input2 = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.hypot(self.input1.value, self.input2.value)
#
#
# class sin(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.sin(self.input.value)
#
#
# class tan(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.tan(self.input.value)
#
#
# class degrees(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.degrees(self.input.value)
#
#
# class radians(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.radians(self.input.value)
#
#
# class acosh(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.acosh(self.input.value)
#
#
# class asinh(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.asinh(self.input.value)
#
#
# class atanh(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.atanh(self.input.value)
#
#
# class cosh(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.cosh(self.input.value)
#
#
# class sinh(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.sinh(self.input.value)
#
#
# class tanh(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.tanh(self.input.value)
#
#
# class erf(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.erf(self.input.value)
#
#
# class erfc(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.erfc(self.input.value)
#
#
# class gamma(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.gamma(self.input.value)
#
#
# class lgamma(_node_model.Node):
#     """"""
#
#     input = _attributes.Float(0, 'input')
#     output = _attributes.Float(0, 'output')
#
#     def run(self):
#         self.output.value = math.lgamma = (x)(self.input.value)

import kukulkan.api.model.node as _node_model
import kukulkan.attributes as _attributes


class Integer(_node_model.Node):
    """An integer typed `Node`."""

    output = _attributes.Integer(0, socket='output', editable=True)


class Float(_node_model.Node):
    """A float typed `Node`."""

    output = _attributes.Float(0.0, socket='output', editable=True)


class Boolean(_node_model.Node):
    """A boolean typed `Node`."""

    output = _attributes.Boolean(False, socket='output', editable=True)


class String(_node_model.Node):
    """A string typed `Node`."""

    output = _attributes.String('', socket='output', editable=True)

"""Logic Nodes."""
import kukulkan.api.model.node as _node_model
import kukulkan.attributes as _attributes


class Branch(_node_model.Node):
    """Branch the execution based on a condition."""
    input = _attributes.Execution(socket='input')
    true = _attributes.Execution(socket='output')
    false = _attributes.Execution(socket='output')
    condition = _attributes.Boolean(False, socket='input')

    def run(self):
        if self.condition:
            self.true.value = True
        else:
            self.false.value = True

import math

import kukulkan.api.model.node as _node_model
import kukulkan.attributes as _attributes


class Pi(_node_model.Node):

    output = _attributes.Float(math.pi, socket='output')


class Exponent(_node_model.Node):

    output = _attributes.Float(math.e, socket='output')

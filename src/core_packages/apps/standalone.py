import kukulkan.api.app

import kukulkan.views


class Kukulkan(kukulkan.api.app.Application):

    def build(self):
        return kukulkan.views.GraphWindow()

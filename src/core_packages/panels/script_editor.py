import logging

from pygments.lexers import PythonLexer
import pygments.token
from PyQt5 import QtWidgets, QtCore, QtGui

import kukulkan.commands
from kukulkan.config import UI


LOGGER = logging.getLogger()


class ScriptEditorPanel(QtWidgets.QDockWidget):
    """A panel to script inside Kukulkan."""

    default_area = QtCore.Qt.RightDockWidgetArea

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setWidget(ScriptEditorWindow())


class ScriptEditorWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setCentralWidget(ScriptEditorWidget())
        self.addToolBar(ScriptEditorToolbar())


class ScriptEditorWidget(QtWidgets.QPlainTextEdit):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.highlighter = ScriptEditorHighlighter(self.document())


class ScriptEditorHighlighter(QtGui.QSyntaxHighlighter):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lexer = PythonLexer()

    def highlightBlock(self, code):
        tokens = self.lexer.get_tokens_unprocessed(code)

        for start, token_type, word in tokens:
            count = len(word)
            if token_type in pygments.token.Text:
                fmt_options = ScriptEditorFormats.text()
            elif token_type in pygments.token.Whitespace:
                fmt_options = ScriptEditorFormats.whitespace()
            elif token_type in pygments.token.Error:
                fmt_options = ScriptEditorFormats.error()
            elif token_type in pygments.token.Other:
                fmt_options = ScriptEditorFormats.other()
            elif token_type in pygments.token.Keyword:
                fmt_options = ScriptEditorFormats.keyword()
            elif token_type in pygments.token.Name:
                fmt_options = ScriptEditorFormats.name()
            elif token_type in pygments.token.Literal:
                if token_type in pygments.token.Literal.String:
                    fmt_options = ScriptEditorFormats.string()
                elif token_type in pygments.token.Literal.Number:
                    fmt_options = ScriptEditorFormats.number()
                else:
                    fmt_options = ScriptEditorFormats.literal()
            elif token_type in pygments.token.String:
                fmt_options = ScriptEditorFormats.string()
            elif token_type in pygments.token.Number:
                fmt_options = ScriptEditorFormats.number()
            elif token_type in pygments.token.Operator:
                fmt_options = ScriptEditorFormats.operator()
            elif token_type in pygments.token.Punctuation:
                fmt_options = ScriptEditorFormats.punctuation()
            elif token_type in pygments.token.Comment:
                fmt_options = ScriptEditorFormats.comment()
            else:
                fmt_options = ScriptEditorFormats.generic()

            fmt_options.setFontPointSize(UI.script_editor.font.size)
            self.setFormat(start, count, fmt_options)


class ScriptEditorFormats:

    @staticmethod
    def text():
        fmt = QtGui.QTextCharFormat()
        return fmt

    @staticmethod
    def whitespace():
        fmt = QtGui.QTextCharFormat()
        return fmt

    @staticmethod
    def error():
        fmt = QtGui.QTextCharFormat()
        fmt.setUnderlineStyle(fmt.DashUnderline)
        fmt.setUnderlineColor(QtGui.QColor(204, 55, 14))
        return fmt

    @staticmethod
    def other():
        fmt = QtGui.QTextCharFormat()
        return fmt

    @staticmethod
    def keyword():
        fmt = QtGui.QTextCharFormat()
        fmt.setForeground(QtGui.QColor(162, 89, 47))
        fmt.setFontWeight(QtGui.QFont.Bold)
        return fmt

    @staticmethod
    def name():
        fmt = QtGui.QTextCharFormat()
        return fmt

    @staticmethod
    def literal():
        fmt = QtGui.QTextCharFormat()
        fmt.setFontItalic(True)
        fmt.setForeground(QtGui.QColor(201, 170, 32))
        return fmt

    @staticmethod
    def string():
        fmt = QtGui.QTextCharFormat()
        fmt.setFontItalic(True)
        fmt.setForeground(QtGui.QColor(201, 170, 32))
        return fmt

    @staticmethod
    def number():
        fmt = QtGui.QTextCharFormat()
        fmt.setForeground(QtGui.QColor(34, 133, 163))
        return fmt

    @staticmethod
    def operator():
        fmt = QtGui.QTextCharFormat()
        fmt.setForeground(QtGui.QColor(206, 70, 221))
        return fmt

    @staticmethod
    def punctuation():
        fmt = QtGui.QTextCharFormat()
        return fmt

    @staticmethod
    def comment():
        fmt = QtGui.QTextCharFormat()
        fmt.setFontItalic(True)
        fmt.setForeground(QtGui.QColor(99, 105, 112))
        return fmt

    @staticmethod
    def generic():
        fmt = QtGui.QTextCharFormat()
        return fmt


class ScriptEditorToolbar(QtWidgets.QToolBar):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.run_action = self.addAction('Run')
        self.run_action.triggered.connect(self.execute_script)

    def execute_script(self):
        script = self.parent().centralWidget().toPlainText()
        return kukulkan.commands.run_script(script)

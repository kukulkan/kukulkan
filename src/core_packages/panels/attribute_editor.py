import logging

from PyQt5 import QtWidgets, QtCore
from blinker import signal

import kukulkan.events
import kukulkan.nodes
import kukulkan.views


LOGGER = logging.getLogger()

class AttributeEditorPanel(QtWidgets.QDockWidget):
    """A panel to visualize the node attributes."""

    default_area = QtCore.Qt.RightDockWidgetArea

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = None
        kukulkan.events.subscribe('self.app.ready', self.initialize)
        self.selection_changed = signal("selection_changed")
        self.selection_changed.connect(self.populate)
        self.initialize()
        self.attribute_widgets = []

    def initialize(self):
        """initialize the Attribute editor with the base widgets and layouts."""
        self.scroll_area = QtWidgets.QScrollArea()
        self.setWidget(self.scroll_area)

    def populate(self, *args, **kwargs):
        """populate the attribute editor with the node information."""
        selection = kukulkan.nodes.selected_nodes

        self.clear()
        if not selection:
            return

        node = selection[-1]
        self.model = node

        self.scroll_widget = QtWidgets.QWidget()
        self.scroll_layout = QtWidgets.QVBoxLayout()
        self.scroll_widget.setLayout(self.scroll_layout)

        self.name_layout = QtWidgets.QHBoxLayout()
        self.scroll_layout.addLayout(self.name_layout)
        self.name_label = QtWidgets.QLabel('Name')
        self.name_line_edit = QtWidgets.QLineEdit()
        self.name_layout.addWidget(self.name_label)
        self.name_layout.addWidget(self.name_line_edit)
        self.name_line_edit.returnPressed.connect(self.update_model_name)
        self.name_line_edit.setText(self.model.name)

        self.attributes_layout = QtWidgets.QVBoxLayout()
        self.scroll_layout.addLayout(self.attributes_layout)

        for attribute in self.model.attributes:
            classes = [x.__name__ for x in attribute.__class__.mro()]
            if 'ExecutionAttribute' not in classes:
                self.add_attribute(attribute)

        self.scroll_area.setWidget(self.scroll_widget)

    def add_attribute(self, attribute):
        widget = getattr(kukulkan.views, attribute.__class__.__name__, None)
        if widget is None:
            for klass in attribute.__class__.mro():
                name = klass.__name__
                widget = getattr(_attribute_widget, name, None)
                if widget:
                    break
            else:
                msg = 'No attribute widget named {}.'
                raise AttributeError(msg.format(self.type))

        attribute_layout = QtWidgets.QHBoxLayout()

        widget = widget(model=attribute)
        label = QtWidgets.QLabel(attribute.name)
        attribute_layout.addWidget(label)
        attribute_layout.addWidget(widget)
        self.attributes_layout.addLayout(attribute_layout)
        self.attribute_widgets.append(widget)

    def clear(self):
        """Clear the attribute editor in case no nodes are selected."""
        widget = self.scroll_area.takeWidget()
        for attribute_widget in self.attribute_widgets:
            attribute_widget.setParent(None)
            attribute_widget.deleteLater()
        if widget is not None:
            widget.deleteLater()
            del widget
        self.model = None
        self.attribute_widgets = []

    def update_model_name(self):
        self.model.name = self.name_line_edit.text()

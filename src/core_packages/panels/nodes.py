import logging

from collections import defaultdict
from functools import partial

from PyQt5 import QtWidgets, QtCore
import kukulkan.events
import kukulkan.nodes
import kukulkan.apps


LOGGER = logging.getLogger(__name__)


class NodesPanel(QtWidgets.QDockWidget):

    def __init__(self, *args, **kwargs):
        super(NodesPanel, self).__init__(*args, **kwargs)
        kukulkan.events.subscribe('app.ready', self.populate)

    def populate(self):
        """Add a button to create a node."""
        self.setWidget(QtWidgets.QWidget())
        self.widget().setLayout(QtWidgets.QVBoxLayout())

        self.categories = defaultdict(list)
        for name, node in kukulkan.nodes.types.items():
            module = getattr(node, '__module__', 'general')
            category = module.split('.')[-1].title()
            self.categories[category].append(name)

        self.tree = NodesTree(self.categories)
        self.widget().layout().addWidget(self.tree)


class NodesTree(QtWidgets.QTreeWidget):

    def __init__(self, categories):
        super().__init__()
        self.setHeaderHidden(True)
        self.setDragEnabled(True)
        size_policy = QtWidgets.QSizePolicy()
        size_policy.setVerticalPolicy(size_policy.MinimumExpanding)
        size_policy.setHorizontalPolicy(size_policy.MinimumExpanding)
        size_policy.setVerticalStretch(1)
        self.setSizePolicy(size_policy)
        self.setStyleSheet('background: transparent')
        for category, nodes in categories.items():
            parent = QtWidgets.QTreeWidgetItem()
            parent.setText(0, category)
            self.addTopLevelItem(parent)
            parent.setExpanded(True)
            for name in nodes:
                item = NodeItem(name)
                parent.addChild(item)

    def mouseDoubleClickEvent(self, event):
        """Create a node of the double clicked type."""
        item = self.itemAt(event.pos())
        if not item or not isinstance(item, NodeItem):
            return super().mouseDoubleClickEvent(event)
        node_type = item.text(0)
        self.create_node(node_type)
        return super().mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)

    def create_node(self, node_type):
        """Create a new node for ``node_type``."""
        window = kukulkan.apps.current.window
        return window.create_new_node(node_type)


class NodeItem(QtWidgets.QTreeWidgetItem):
    """Node type items class.

    Allows for easier type checking inside the `QTreeWidget`.

    :param name: Text to display on this item.
    """

    def __init__(self, name):
        super().__init__()
        self.setText(0, name)
        self.setFlags(
            QtCore.Qt.ItemIsSelectable
            | QtCore.Qt.ItemIsEnabled
            | QtCore.Qt.ItemIsDragEnabled
            | QtCore.Qt.ItemIsDragEnabled
        )

    def dropEvent(self, event):
        super().dropEvent(event)

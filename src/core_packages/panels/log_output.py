import logging

from PyQt5 import QtWidgets, QtCore


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class LogOutputPanel(QtWidgets.QDockWidget):
    """A panel to visualize logging output."""

    default_area = QtCore.Qt.BottomDockWidgetArea

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setWidget(OutputWidget())


class OutputWidget(QtWidgets.QPlainTextEdit):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setReadOnly(True)
        self.handler = OutputHandler(self)
        LOGGER.addHandler(self.handler)


class OutputHandler(logging.Handler):

    def __init__(self, widget, level=logging.NOTSET):
        super().__init__(level)
        self.widget = widget
        self.setFormatter(OutputFormatter())

    def emit(self, record):
        if not self.widget:
            return
        message = self.format(record=record)
        self.widget.appendHtml(message)


class OutputFormatter(logging.Formatter):

    def __init__(self):
        fmt = '%(asctime)s, %(name)s: %(message)s'
        datefmt = '%I:%M:%S'
        super().__init__(fmt=fmt, datefmt=datefmt)

    def format(self, record):
        msg = super().format(record=record)
        start_tag = '<p style="white-space: pre-wrap; color:{}">'
        end_tag = '</p>'
        if record.levelno <= logging.DEBUG:
            color = '#0DCC69'
        elif record.levelno <= logging.INFO:
            color = '#EDEDED'
        elif record.levelno <= logging.WARNING:
            color = '#CCA60D'
        else:
            color = '#CC370E'
        return start_tag.format(color) + msg + end_tag

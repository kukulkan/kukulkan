import abc
from kukulkan.config import UI

from PyQt5 import QtWidgets
from PyQt5 import QtCore

from . import plug as _plug
from .. import attribute_widget as _attribute_widget


class Attribute(QtWidgets.QGraphicsItem):

    def __init__(self, name, node, model):
        super(Attribute, self).__init__(node)
        self.name = name
        self.node = node
        self.plug = None
        self.base_widget = None
        self.model = model
        self.type = model.__class__.__name__

        self.is_input = False
        self.is_output = False

        self.initialize()

        self.reset()
        self._create_plug()
        self.create_base_widget()
        classes = [x.__name__ for x in self.model.__class__.mro()]
        if 'ExecutionAttribute' not in classes:
            self.model.editable_set.connect(self.set_widget_lock_state)
            if not self.model.editable:
                self.lock_widget()

    def __str__(self):
        """Return the attribute string form."""
        return '.'.join(map(str, [self.node, self.name]))

    @abc.abstractmethod
    def initialize(self):
        """Override this in subclasses to set is_input/is_output."""

    @property
    def connections(self):
        """Return the connections of this `Attribute`."""
        connections = {}
        if self.plug is not None:
            connections.update(self.plug.connections)
            return connections

    def reset(self):
        """Reset the graphic state to its initial value."""
        self.x = 0
        self.y = 0
        self.size = UI.attribute.size
        self.width = self.node.width + self.size
        self.widget_x = self.x + self.size

    def boundingRect(self):
        return QtCore.QRectF(
            self.x,
            self.y,
            self.width,
            self.size,
        )

    def create_base_widget(self):
        base_widget = getattr(_attribute_widget, self.type, None)
        if base_widget is None:
            for klass in self.model.__class__.mro():
                name = klass.__name__
                base_widget = getattr(_attribute_widget, name, None)
                if base_widget:
                    break
            else:
                msg = 'No attribute widget named {}.'
                raise AttributeError(msg.format(self.type))
        self.base_widget = base_widget(self.name, self.node, self)
        self.base_widget = self.node.scene().addWidget(self.base_widget)
        self.base_widget.setParentItem(self)
        widget_height = self.base_widget.widget().height()
        widget_offset = (self.size - widget_height) / 2
        self.base_widget.setPos(
            self.widget_x,
            self.y + widget_offset
        )

    def lock_widget(self):
        if self.base_widget.widget().widget:
            self.base_widget.widget().widget.setEnabled(False)
            self.base_widget.widget().label.setEnabled(False)

    def unlock_widget(self):
        if self.base_widget.widget().widget:
            self.base_widget.widget().widget.setEnabled(True)
            self.base_widget.widget().label.setEnabled(True)

    def set_widget_lock_state(self, should_lock):
        if should_lock:
            self.lock_widget()
        else:
            self.unlock_widget()

    def paint(self, painter, option, widget):
        pass

    @abc.abstractmethod
    def _create_plug(self):
        """Create the default plug for this `Attribute`.

        Override this in the subclasses
        """


class Input(Attribute):
    def initialize(self, *args, **kwargs):
        super(Input, self).initialize(*args, **kwargs)
        self.is_input = True

    def _create_plug(self):
        """Create an input plug."""
        self.plug = _plug.Input(self)


class Output(Attribute):
    def initialize(self, *args, **kwargs):
        super(Output, self).initialize(*args, **kwargs)
        self.is_output = True

    def _create_plug(self):
        """Create an input plug."""
        self.plug = _plug.Output(self)
        self.plug.setPos(
            self.width - self.size,
            self.y
        )

from kukulkan.config import UI

from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui

from . import attribute as _attribute

import kukulkan.nodes
import logging

LOGGER = logging.getLogger(__name__)


class Node(QtWidgets.QGraphicsItem):
    """A node item that can be added to a graph."""

    def __init__(self, model, scene=None):
        super(Node, self).__init__()
        if scene is not None:
            scene.addItem(self)
        self.name = model.name
        self.node_type = model.__class__.__name__
        self.model = model
        self.width = UI.node.width
        self.attributes = {}
        self.plugs = {}
        self.reset()
        self.create_attributes()
        self.model.name_set.connect(self.update_name)
        self.model.attribute_added.connect(self.add_attribute)
        self.model.deleted.connect(self.on_model_deleted)

    def __str__(self):
        """Return the string form of the node (its name)."""
        return str(self.name)

    def update_name(self, new_name):
        self.name = new_name
        self.scene().update(QtCore.QRectF(self.boundingRect()))

    def on_model_deleted(self, *args, **kwargs):
        for attribute in self.attributes.values():
            plug = attribute.plug
            for connection in list(plug.connections.keys()):
                plug.remove_connection(connection)
        self.scene().removeItem(self)

    @property
    def connections(self):
        """Return the connections of this `Node`."""
        connections = {}
        for attribute in self.attributes.values():
            connections.update(attribute.connections)
        return connections

    def reset(self):
        """Reset the graphic state to its initial value."""
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsFocusable)

        self.resizing = False

        self.x = 0
        self.y = 0

        self.highlighter = QtGui.QPainterPathStroker()
        self.highlighter.setCapStyle(QtCore.Qt.RoundCap)
        self.resize_handle_size = 10

    def create_attributes(self):
        if self.model is None:
            return
        for attribute in self.model.attributes:
            self.add_attribute(attribute)

    @property
    def base_height(self):
        return (
            UI.node.header.height
            + UI.attribute.spacing
            + self.resize_handle_size
        )

    @property
    def height(self):
        return (
            self.base_height
            + UI.attribute.spacing * len(self.attributes)
            + UI.attribute.size * (len(self.attributes) - 1)
        )

    def add_attribute(self, attr):
        name = attr.name
        if name in self.attributes:
            raise KeyError('{} already exists.'.format(self.attributes[name]))

        socket = attr.socket
        if socket == 'input':
            attribute = _attribute.Input
        elif socket == 'output':
            attribute = _attribute.Output
        else:
            return

        try:
            attribute = attribute(name, self, attr)
        except AttributeError as err:
            LOGGER.error(err)
            return

        attribute.setParentItem(self)
        self.attributes[name] = attribute
        attr_x = attribute.size * -0.5
        attr_y = (
            UI.node.header.height
            + UI.attribute.spacing * len(self.attributes)
            + attribute.size * (len(self.attributes) - 1)
        )
        attribute.setPos(attr_x, attr_y)
        return attribute

    def boundingRect(self):
        return QtCore.QRectF(
            self.x - UI.node.highlight.padding,
            self.y - UI.node.highlight.padding,
            UI.node.width + UI.node.highlight.padding * 2,
            self.height + UI.node.highlight.padding * 2,
        )

    def paint(self, painter, option, widget):
        if self.isSelected():
            # self.model.is_selected = True
            self.paint_highlight(painter, option, widget)
        else:
            # self.model.is_selected = False
            self.paint_outline(painter, option, widget)
        self.paint_clip(painter, option, widget)
        self.paint_body(painter, option, widget)
        self.paint_label(painter, option, widget)
        # self.paint_resize_handle(painter, option, widget)
        self.paint_label_text(painter, option, widget)
        self.paint_label_type(painter, option, widget)

    def paint_outline(self, painter, option, widget):
        """Adds a outline to the node."""
        path = QtGui.QPainterPath()
        path.addRoundedRect(
            self.x,
            self.y,
            UI.node.width,
            self.height,
            UI.node.roundness,
            UI.node.roundness,
        )
        self.highlighter.setWidth(UI.node.highlight.padding * 2)
        outline = self.highlighter.createStroke(path)
        color = QtGui.QColor(*UI.node.outline.brush)
        painter.fillPath(outline, QtGui.QBrush(color))

    def paint_highlight(self, painter, option, widget):
        """Highlight the node.

        Called when the node is selected.
        """
        path = QtGui.QPainterPath()
        path.addRoundedRect(
            self.x,
            self.y,
            UI.node.width,
            self.height,
            UI.node.roundness,
            UI.node.roundness,
        )
        self.highlighter.setWidth(UI.node.highlight.padding * 2)
        outline = self.highlighter.createStroke(path)
        color = QtGui.QColor(*UI.node.highlight.brush)
        painter.fillPath(outline, QtGui.QBrush(color))

    def paint_clip(self, painter, option, widget):
        # Clip the node to make it round.
        clip = QtGui.QPainterPath()
        clip.addRoundedRect(
            self.x,
            self.y,
            UI.node.width,
            self.height,
            UI.node.roundness,
            UI.node.roundness,
        )
        painter.setClipping(True)
        painter.setClipPath(clip)

        painter.setPen(QtCore.Qt.NoPen)

    def paint_label(self, painter, option, widget):
        # Paint the label part.
        painter.setBrush(QtGui.QBrush(QtGui.QColor(*UI.node.header.brush)))
        painter.drawRect(
            self.x,
            self.y,
            UI.node.width,
            UI.node.header.height,
        )

    def paint_body(self, painter, option, widget):
        # Paint the body part.
        painter.setBrush(QtGui.QBrush(QtGui.QColor(*UI.node.body.brush)))
        painter.drawRect(
            self.x,
            self.y + UI.node.header.height,
            UI.node.width,
            self.height - UI.node.header.height,
        )

    def paint_label_text(self, painter, option, widget):
        # Paint the label text
        font = QtGui.QFont(
            UI.node.label.font.family,
            UI.node.label.font.size,
            UI.node.label.font.weight,
        )
        painter.setFont(font)
        painter.setPen(QtGui.QColor(*UI.node.label.pen))
        painter.drawText(
            self.x + UI.node.label.offset,
            self.y,
            UI.node.width - UI.node.label.offset,
            UI.node.header.height,
            QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft,
            self.name,
        )

    def paint_label_type(self, painter, option, widget):
        # Paint the label type
        font = QtGui.QFont(
            UI.node.label.font.family,
            UI.node.label.font.size,
            UI.node.label.font.weight,
        )
        painter.setFont(font)
        painter.setPen(QtGui.QColor(*UI.node.label.pen))
        painter.drawText(
            self.x + UI.node.label.offset,
            self.y,
            UI.node.width - UI.node.label.offset * 2,
            UI.node.header.height,
            QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight,
            self.node_type.title(),
        )

    def paint_resize_handle(self, painter, option, widget):
        """Paint the footer region of the node.
        """
        painter.setBrush(QtGui.QBrush(QtGui.QColor(*UI['header']['brush'])))
        painter.drawRect(
            self.x,
            self.y + self.height - self.resize_handle_size,
            UI.node.width,
            self.resize_handle_size,
        )

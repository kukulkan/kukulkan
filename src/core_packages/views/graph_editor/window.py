import random

from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore

import kukulkan.api.command
import kukulkan.gui.theme as _theme
import kukulkan.nodes
import kukulkan.commands
from . import graph as _graph


NODE_TYPES = list(kukulkan.nodes.types.keys())


class GraphWindow(QtWidgets.QMainWindow):
    """A node editor window."""

    def __init__(self, *args, **kwargs):
        super(GraphWindow, self).__init__(*args, **kwargs)
        self.theme_manager = _theme.ThemeManager()
        self.build()

    def build(self):
        self.view = _graph.GraphView()
        self.scene = _graph.Graph()
        self.view.setScene(self.scene)
        self.view.setRenderHints(QtGui.QPainter.Antialiasing)
        self.setCentralWidget(self.view)
        self.setTabPosition(
            QtCore.Qt.LeftDockWidgetArea,
            QtWidgets.QTabWidget.West
        )
        self.setTabPosition(
            QtCore.Qt.RightDockWidgetArea,
            QtWidgets.QTabWidget.East
        )
        self.setTabPosition(
            QtCore.Qt.TopDockWidgetArea,
            QtWidgets.QTabWidget.North
        )
        self.setTabPosition(
            QtCore.Qt.BottomDockWidgetArea,
            QtWidgets.QTabWidget.North
        )

    def keyPressEvent(self, event):
        """Create or delete a node."""
        key = event.key()
        if key == QtCore.Qt.Key_Delete:
            self.delete_selected_nodes()
        elif key == QtCore.Qt.Key_N:
            random_index = random.randint(0, len(NODE_TYPES) - 1)
            node_type = NODE_TYPES[random_index]
            self.create_new_node(node_type)
            self.delete_selected_nodes()
        elif key == QtCore.Qt.Key_Z:
            if event.modifiers() == QtCore.Qt.ShiftModifier:
                kukulkan.api.command.redo()
            elif event.modifiers() == QtCore.Qt.NoModifier:
                kukulkan.api.command.undo()
        return QtWidgets.QMainWindow.keyPressEvent(self, event)

    def create_new_node(self, node_type):
        """Add a new node to the scene."""
        count = len(set(i.topLevelItem() for i in self.scene.items()))
        name = 'Node{:02d}'.format(count)
        node = kukulkan.commands.create_node(name, node_type)
        return node

    def delete_selected_nodes(self):
        """Delete the nodes currently selected."""
        with kukulkan.api.command.CommandStack():
            for item in self.scene.items():
                if item.isSelected():
                    kukulkan.commands.DeleteNode(item.model)

import sys
import kukulkan.events
import kukulkan.config.theme
from kukulkan.config import UI

from PyQt5 import QtWidgets
from PyQt5 import QtCore


class AttributeWidget(QtWidgets.QWidget):
    def __init__(self, name, node, attribute, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.node = node
        self.attribute = attribute
        self.widget = None
        self.reset()
        self.create_widget()
        style_sheet = kukulkan.config.theme.get_style_sheet(UI.theme)
        if self.widget:
            self.attribute.model.value_set.connect(self.update_widget_value)
            self.update_widget_value(self.attribute.model)
            self.left_layout.addWidget(self.widget)
            if self.attribute.is_input:
                self.left_layout.addWidget(self.widget)
                self.right_layout.addWidget(self.label)
                self.right_layout.setAlignment(QtCore.Qt.AlignLeft)
                self.right_layout.addStretch()
            elif self.attribute.is_output:
                self.left_layout.addWidget(self.label)
                self.right_layout.addWidget(self.widget)
                self.left_layout.setAlignment(QtCore.Qt.AlignRight)
                self.left_layout.addStretch()
                self.left_layout.setDirection(QtWidgets.QHBoxLayout.RightToLeft)

            self.widget.setStyleSheet(style_sheet)
        else:
            if self.attribute.is_input:
                self.right_layout.addWidget(self.label)
                self.right_layout.setAlignment(QtCore.Qt.AlignLeft)
                self.right_layout.addStretch()
            elif self.attribute.is_output:
                self.left_layout.addWidget(self.label)
                self.left_layout.setAlignment(QtCore.Qt.AlignRight)
                self.left_layout.addStretch()
                self.left_layout.setDirection(QtWidgets.QHBoxLayout.RightToLeft)

        if self.label:
            self.label.setStyleSheet(style_sheet)

        kukulkan.events.subscribe(
            'style_sheet.changed',
            self.on_style_sheet_changed,
        )

    def on_style_sheet_changed(self, style_sheet):
        """Called when the application style sheet is modified.

        Passed value is the style sheet string.
        """
        if self.widget:
            self.widget.setStyleSheet(style_sheet)
        if self.label:
            self.label.setStyleSheet(style_sheet)

    def reset(self):
        self.myWidth = self.node.width - self.attribute.size
        self.myHeight = 50
        self.setStyleSheet("background-color: transparent")
        self.resize(self.myWidth, self.myHeight)
        self.layout = QtWidgets.QHBoxLayout()
        self.setLayout(self.layout)

        self.left_layout = QtWidgets.QHBoxLayout()
        self.right_layout = QtWidgets.QHBoxLayout()
        self.left_layout.setAlignment(QtCore.Qt.AlignLeft)
        self.right_layout.setAlignment(QtCore.Qt.AlignRight)
        self.layout.addLayout(self.left_layout)
        self.layout.addLayout(self.right_layout)

        self.label = QtWidgets.QLabel(self.name)
        self.right_layout.addWidget(self.label)

    def create_widget(self):
        """Create the widget of this widget.

        Override when subclassing to add a widget to the attribute.
        You want to set the variable self.widget like so:
            self.widget = QtWidgets.QSpinBox()
        """

    def update_model_value(self):
        """Update the value of this container widget.

        This method should be called by a signal of the contained widget.
        It should also be override in the subclasses.
        """

    def update_widget_value(self, model):
        """Update the value of this contained widget.

        override this method in the subclasses
        """


class Message(AttributeWidget):
    """Simple attribute that is used to connect nodes."""

class Execution(AttributeWidget):
    """Simple attribute that is used to connect nodes."""


class Numeric(AttributeWidget):
    """Base class for numeric attributes."""

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setValue(model.value)

    def update_model_value(self):
        super().update_model_value()
        self.attribute.model.value = self.widget.value()


class Integer(Numeric):
    """Int attribute."""
    def create_widget(self):
        """Create a QSpinBox."""
        super().create_widget()
        self.widget = QtWidgets.QSpinBox()
        size = self.node.boundingRect().width()/3
        self.widget.setMinimumWidth(size)
        self.widget.setRange(-2147483648, 2147483647)
        self.widget.valueChanged.connect(self.update_model_value)

    def update_model_value(self):
        value = self.widget.value()
        self.attribute.model.value = value


class Float(Numeric):
    """Float attribute."""
    def create_widget(self):
        """Create a QDoubleSpinBox."""
        super().create_widget()
        self.widget = QtWidgets.QDoubleSpinBox()
        size = self.node.boundingRect().width()/3
        self.widget.setMinimumWidth(size)
        self.widget.setRange(float("-inf"), float('inf'))
        self.widget.valueChanged.connect(self.update_model_value)


class Boolean(AttributeWidget):
    """Bool attribute."""
    def create_widget(self):
        """Create a QCheckBox."""
        super().create_widget()
        self.widget = QtWidgets.QCheckBox()
        self.widget.setTristate(False)
        self.widget.stateChanged.connect(self.update_model_value)

    def update_model_value(self):
        super().update_model_value()
        self.attribute.model.value = self.widget.isChecked()

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setChecked(bool(model.value))


class String(AttributeWidget):
    """String attribute."""
    def create_widget(self):
        """Create a QLineEdit."""
        super().create_widget()
        self.widget = QtWidgets.QLineEdit()
        self.widget.editingFinished.connect(self.update_model_value)

    def update_model_value(self):
        super().update_model_value()
        self.attribute.model.value = self.widget.text()

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setText(str(model.value))


class Enum(AttributeWidget):
    """Enum attribute."""
    def create_widget(self):
        """Create a QComboBox."""
        super().create_widget()
        self.widget = QtWidgets.QComboBox()
        self.widget.currentIndexChanged.connect(self.update_model_value)

    def update_model_value(self):
        super().update_model_value()
        self.attribute.model.value = self.widget.currentIndex()

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setCurrentIndex(model.value)

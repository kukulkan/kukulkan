from PyQt5 import QtWidgets

import kukulkan.events
import kukulkan.config.theme
from kukulkan.config import UI


class AttributeWidget(QtWidgets.QWidget):
    def __init__(self, model, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.widget = None
        self.create_widget()
        self.model = model

        style_sheet = kukulkan.config.theme.get_style_sheet(UI.theme)
        if self.widget:
            self.layout = QtWidgets.QHBoxLayout()
            self.setLayout(self.layout)
            self.layout.addWidget(self.widget)
            self.model.value_set.connect(self.update_widget_value)
            self.update_widget_value(self.model)
            self.widget.setStyleSheet(style_sheet)

        kukulkan.events.subscribe(
            'style_sheet.changed',
            self.on_style_sheet_changed,
        )

    def deleteLater(self, *args, **kwargs):
        """Disconnect the event that updates the widget value."""
        self.model.value_set.disconnect(self.update_widget_value)
        super().deleteLater(*args, **kwargs)

    def on_style_sheet_changed(self, style_sheet):
        """Called when the application style sheet is modified.

        Passed value is the style sheet string.
        """
        if self.widget:
            self.widget.setStyleSheet(style_sheet)

    def create_widget(self):
        """Create the widget of this widget.

        Override when subclassing to add a widget to the attribute.
        You want to set the variable self.widget like so:
            self.widget = QtWidgets.QSpinBox()
        """

    def update_model_value(self):
        """Update the value of this container widget.

        This method should be called by a signal of the contained widget.
        It should also be override in the subclasses.
        """

    def update_widget_value(self, model):
        """Update the value of this contained widget.

        override this method in the subclasses
        """


class Numeric(AttributeWidget):
    """Base class for numeric attributes."""

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setValue(model.value)

    def update_model_value(self):
        super().update_model_value()
        self.model.value = self.widget.value()

class Integer(Numeric):
    """Int attribute."""
    def create_widget(self):
        """Create a QSpinBox."""
        super().create_widget()
        self.widget = QtWidgets.QSpinBox()
        self.widget.setRange(-2147483648, 2147483647)
        self.widget.valueChanged.connect(self.update_model_value)

class Float(Numeric):
    """Float attribute."""
    def create_widget(self):
        """Create a QDoubleSpinBox."""
        super().create_widget()
        self.widget = QtWidgets.QDoubleSpinBox()
        self.widget.setRange(float("-inf"), float('inf'))
        self.widget.valueChanged.connect(self.update_model_value)

class Boolean(AttributeWidget):
    """Bool attribute."""
    def create_widget(self):
        """Create a QCheckBox."""
        super().create_widget()
        self.widget = QtWidgets.QCheckBox()
        self.widget.setTristate(False)
        self.widget.stateChanged.connect(self.update_model_value)

    def update_model_value(self):
        super().update_model_value()
        self.model.value = self.widget.isChecked()

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setChecked(bool(model.value))

class String(AttributeWidget):
    """String attribute."""
    def create_widget(self):
        """Create a QLineEdit."""
        super().create_widget()
        self.widget = QtWidgets.QLineEdit()
        self.widget.editingFinished.connect(self.update_model_value)

    def update_model_value(self):
        super().update_model_value()
        self.model.value = self.widget.text()

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setText(str(model.value))


class Enum(AttributeWidget):
    """Enum attribute."""
    def create_widget(self):
        """Create a QComboBox."""
        super().create_widget()
        self.widget = QtWidgets.QComboBox()
        self.widget.currentIndexChanged.connect(self.update_model_value)

    def update_model_value(self):
        super().update_model_value()
        self.model.value = self.widget.currentIndex()

    def update_widget_value(self, model):
        super().update_widget_value(model)
        self.widget.setCurrentIndex(model.value)

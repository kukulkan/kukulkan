import click

import kukulkan.apps


@click.command()
@click.option(
    '--app',
    default='Kukulkan',
    help='Kukulkan application class name.',
)
def run_app(app):
    """Run Kukulkan"""
    app_class = kukulkan.apps.types.get(app, None)
    if app_class is None:
        raise ValueError(
            '`app` argument should be a valid Kukulkan application class name.'
        )
    return app_class().run()

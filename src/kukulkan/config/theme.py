"""Generate themes by parsing template files."""
import re
import pkgutil

from kukulkan.config.collector import (
    merge_data,
    get_configuration_file_data,
)
from kukulkan.config.reader import DataReader


_THEME_TAG = '@theme.'
_TAG_RE = _THEME_TAG + r'(?P<key>[\.a-zA-z\-_]+)'


def get_default_theme():
    """Return the default theme configuration data.

    :rtype: dict
    """
    return get_configuration_file_data('default', 'themes')


def get_template():
    """Return the style sheet template string.

    :rtype: str
    """
    return pkgutil.get_data('kukulkan.gui.templates', 'app.qss').decode()


def get_template_tags():
    """Return tags to replace by themes values."""
    return re.findall(_TAG_RE, get_template())


def _parse_value(value):
    """Parse ``value`` and return a new one if needed.

    This is to handle CSON values translating badly to QSS properties,
    like colors for example.
    """
    is_iterable = hasattr(value, '__iter__')
    if is_iterable and not isinstance(value, basestring):
        if len(value) == 3:
            return _parse_rgb(value)
        elif len(value) == 4:
            return _parse_rgba(value)
    return value


def _parse_rgb(value):
    """Replace a color lists by a regular QSS rgb string.

    :param value: An iterable of three values.
    :type value: list(str)
    :rtype: str
    """
    return 'rgb({c[0]}, {c[1]}, {c[2]})'.format(c=value)


def _parse_rgba(value):
    """Replace a color lists by a regular QSS rgba string.

    :param value: An iterable of four values.
    :type value: list(str)
    :rtype: str
    """
    return 'rgb({c[0]}, {c[1]}, {c[2]}, {c[3]})'.format(c=value)


def _resolve_style_sheet(data):
    """Fill all tags by real values."""
    resolved = get_template()
    for key, value in data.items():
        tag_name = _THEME_TAG + key
        parsed = _parse_value(value)
        resolved = resolved.replace(tag_name, parsed)
    return resolved


def get_style_sheet(name):
    """Return a theme style sheet by name.

    :param str name: Name of the theme.
    :rtype: str
    """
    tags = get_template_tags()
    data = get_default_theme()
    if name != 'default':
        theme = get_configuration_file_data(name, folder='themes')
        data = merge_data(data, theme)
    reader = DataReader(data)
    theme_data = {}
    for key in tags:
        tokens = key.split('.')
        value = reader
        for token in tokens:
            value = getattr(value, token)
        theme_data[key] = value
    return _resolve_style_sheet(theme_data)

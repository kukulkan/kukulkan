import kukulkan.events
from kukulkan.config.collector import (
    get_configuration_files,
    get_configuration_file_data,
)
from kukulkan.config.watchers import FileWatcher


class DataReader(object):
    """A configuration data reader.

    :param dict data: Data to parse.
    """

    def __init__(self, data={}):
        self.data = Root()
        self.raw_data = data
        self._visit_raw_data(data)

    def update(self, data):
        """Update this reader with new data."""
        self.raw_data = data
        self._visit_raw_data(self.raw_data)

    def __getattr__(self, name):
        if name in self.data.children:
            return self.data.children[name]
        err = 'ConfigReader object has no attribute {}.'.format(name)
        raise AttributeError(err)

    def _visit_raw_data(self, root, parent=None):
        """Recursively visit a tree."""
        if parent is None:
            parent = self.data
        for key, value in root.items():
            if isinstance(value, dict):
                item = Item(value, parent)
                self._visit_raw_data(value, item)
            else:
                item = value
            parent.children[key] = item


class ConfigReader(DataReader):
    """A configuration file reader.

    :param str name: Name of the configuration file.
    :param str folder: Optional name of a configuration sub-folder.
    :raise ValueError: When ``name`` does not refer to a valid
                       configuration file name.
    """

    def __init__(self, name, folder=None):
        self.name = name
        self.folder = folder
        self.paths = get_configuration_files(
            name,
            self.folder,
            candidate=True
        )
        self.raw_data = {}
        self.data = Root()
        self.update()
        self.watchers = []
        self._setup_watchers()
        kukulkan.events.subscribe(
            self.file_event_key + '.changed',
            self.update,
        )

    def update(self):
        """Read the configuration file and update this reader data."""
        self.raw_data = get_configuration_file_data(
            self.name,
            self.folder,
            candidate=True,
        )
        self._visit_raw_data(self.raw_data)
        kukulkan.events.notify(self.event_key + '.changed')

    def _setup_watchers(self):
        """Create `FileWatcher` to keep track of changes."""
        for path in self.paths:
            watcher = FileWatcher(self.file_event_key, path)
            watcher.setDaemon(True)
            watcher.start()
            self.watchers.append(watcher)

    @property
    def file_event_key(self):
        """Return the event key this ConfigReader watches.

        :rtype: str
        """
        return '.'.join([
            'file',
            self.event_key,
        ])

    @property
    def event_key(self):
        """Return the event key this ConfigReader sends.

        :rtype: str
        """
        return '.'.join([
            self.folder or 'config',
            self.name,
        ])


class Item(object):
    """A `ConfigReader` item."""

    def __init__(self, value, parent=None):
        self.value = value
        self.parent = parent
        self.children = {}

    def __getattr__(self, name):
        if name in self.children:
            return self.children[name]
        try:
            return getattr(self.value, name)
        except AttributeError:
            raise AttributeError(
                'Item object has no attribute {}.'.format(name)
            )

    def __repr__(self):
        return repr(self.value)

    def __str__(self):
        return str(self.value)


class Root(Item):
    """A `ConfigReader` root."""

    def __init__(self):
        self.parent = None
        self.value = self.children = {}

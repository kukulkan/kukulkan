"""A simple publisher-observer implementation for event handling."""

import logging

from collections import defaultdict


LOGGER = logging.getLogger(__name__)
PUBLISHERS = defaultdict(list)


def subscribe(name, func):
    """Subscribe to a publisher.

    :param name: Name of the publisher to observe.
    :param func: Callable subscribing.
    :type name: str
    :type func: callable
    """
    PUBLISHERS[name].append(func)
    LOGGER.debug('%s subscribed to %s.', func.__name__, name)


def unsubscribe(name, func):
    """Unsubscribe from a publisher.

    :param name: Name of the publisher.
    :param func: Callable subscribing.
    :type name: str
    :type func: callable
    """
    if func not in PUBLISHERS[name]:
        return
    PUBLISHERS[name].remove(func)
    LOGGER.debug('%s unsubscribed from %s.', func.__name__, name)


def notify(name, *args, **kwargs):
    """Notify all observers.

    :param name: Name of the publisher emitting.
    :type name: str
    """
    LOGGER.debug('Emitted %s:', name)
    if name not in PUBLISHERS:
        return
    for func in PUBLISHERS[name]:
        func(*args, **kwargs)
        LOGGER.debug('\t%s was notified.', func.__name__)

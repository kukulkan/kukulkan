import pkg_resources


namespace = 'kukulkan.extensions.apps'


types = {}
for entry_point in pkg_resources.iter_entry_points(namespace):
    types[entry_point.name] = entry_point.load()
    globals()[entry_point.name] = entry_point.load()


current = None


__all__ = ['current'] + list(types.keys())

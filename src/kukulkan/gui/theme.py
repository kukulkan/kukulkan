import os

import kukulkan.events
import kukulkan.config.theme
import kukulkan.config.watchers
from kukulkan.config import UI


class ThemeManager(object):
    """Manage the application style sheet."""

    def __init__(self):
        self.theme = None
        self.watcher = None
        kukulkan.events.subscribe('config.ui.changed', self.on_theme_changed)
        kukulkan.events.subscribe('app.ready', self.on_theme_changed)
        kukulkan.events.subscribe('app.ready', self.setup_watcher)

    def on_theme_changed(self):
        new_theme = UI.theme
        if new_theme == self.theme:
            return
        old_theme_event = 'themes.{}.changed'.format(self.theme)
        kukulkan.events.unsubscribe(
            old_theme_event,
            self.on_style_sheet_changed
        )
        self.theme = new_theme
        theme_event = 'themes.{}.changed'.format(new_theme)
        kukulkan.events.subscribe(
            theme_event,
            self.on_style_sheet_changed
        )
        self.on_style_sheet_changed()

    def on_style_sheet_changed(self):
        style = kukulkan.config.theme.get_style_sheet(self.theme)
        kukulkan.events.notify('style_sheet.changed', style)

    def setup_watcher(self):
        import kukulkan.gui
        gui_path = kukulkan.gui.__path__
        if not gui_path:
            return
        template = os.path.join(gui_path[0], 'templates', 'app.qss')
        self.watcher = kukulkan.config.watchers.FileWatcher(
            'app.template',
            template,
        )
        self.watcher.setDaemon(True)
        self.watcher.start()
        kukulkan.events.subscribe(
            'app.template.changed',
            self.on_style_sheet_changed,
        )

import logging


LOGGER = logging.getLogger(__name__)


class LogErrors:
    """Suppress and log errors in decorated function."""

    def __init__(self, func, logger=None):
        self.func = func
        self.logger = logger
        if self.logger is None:
            self.logger = LOGGER

    def __call__(self, *args, **kwargs):
        res = None
        try:
            res = self.func(*args, **kwargs)
        except Exception as err:
            self.logger.error(str(err))
        return res

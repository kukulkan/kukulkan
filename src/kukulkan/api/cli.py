import pkg_resources

import click


def get_commands():
    """Return all commands available.

    Returned value is a dict of command class name, entry point pair.

    Entry points are not loaded on the fly, you have to manually load them
    with the ``.load()`` method.
    """
    ret = {}
    for ep in pkg_resources.iter_entry_points('kukulkan.extensions.cli'):
        ret[ep.name] = ep
    return ret


class KukulkanCLI(click.MultiCommand):
    """Groups all extensions commands under a kukulkan command."""

    def list_commands(self, ctx):
        return sorted(get_commands().keys())

    def get_command(self, ctx, name):
        ep = get_commands().get(name, None)
        if ep is None:
            raise ValueError('Please provide a valid Kukulkan command.')
        return ep.load()


@click.command(cls=KukulkanCLI)
def cli():
    """Kukulkan command-line interface."""

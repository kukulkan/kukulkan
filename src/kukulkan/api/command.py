import logging
import abc
from collections import defaultdict


LOGGER = logging.getLogger(__name__)


__STACK_COMMANDS__ = defaultdict(list)
__UNDO_QUEUE__ = []
__REDO_QUEUE__ = []


class CommandStack:
    """Register commands to run.

    `CommandStack` instances are in charge of running the `Command`
    objects they contain.

    They are also responsible for doing a clean undo/redo.

    :attribute commands: Commands this `CommandStack` contains.
    :attribute stack: Stack this `CommandStack` belongs to.
    :attribute index: Current position in the `CommandStack`.
    :type commands: list(Command)
    :type stack: CommandStack
    :type index: int
    """

    def __init__(self):
        self.commands = []
        self.stack = None
        self.index = -1
        self.name = None
        __UNDO_QUEUE__.append(self)

    def __enter__(self, name=None):
        self.name = name
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.commands = __STACK_COMMANDS__[self]
        self.run()

    def run(self):
        """Run all `Command` objects of this stack."""
        while self.index < len(self.commands) - 1:
            self.run_one()

    def undo(self):
        """Undo the whole stack."""
        while self.index > -1:
            self.undo_one()

    def redo(self):
        """Redo the whole stack."""
        while self.index < len(self.commands) - 1:
            self.redo_one()

    def run_one(self):
        """Run the next `Command` of the stack."""
        cmd = self.commands[self.index]
        cmd.run()
        cmd.has_run = True
        self.index += 1

    def undo_one(self):
        """Undo the previously ran `Command` of the stack."""
        cmd = self.commands[self.index]
        cmd.undo()
        self.index -= 1

    def redo_one(self):
        """Redo the previously undone `Command` of the stack."""
        cmd = self.commands[self.index]
        cmd.redo()
        self.index += 1


class Command:
    """A single command that can be executed in Kukulkan.

    :attribute args: Positional arguments specified for this command.
    :attribute kwargs: Keyword arguments specified for this command.
    :attribute has_run: `True` if the command has run at least once.
    :attribute stack: Stack this `Command` belongs to.
    :type args: tuple
    :type kwargs: dict
    :type has_run: bool
    :type stack: CommandStack
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.has_run = False
        if __UNDO_QUEUE__:
            self.stack = __UNDO_QUEUE__[-1]
            __STACK_COMMANDS__[self.stack].append(self)
        else:
            self.stack = None

    @abc.abstractmethod
    def run(self):
        """Perform the changes."""

    def undo(self):
        """Undo the changes made by the `Command.do` method."""

    def redo(self):
        """Redo the changes undone by the `Command.undo` method."""
        self.run()


def undo():
    """Undo the last command stack."""
    try:
        stack = __UNDO_QUEUE__.pop()
    except IndexError:
        LOGGER.error('No more command to undo.')
        return
    stack.undo()
    __REDO_QUEUE__.append(stack)


def redo():
    """Redo the last command stack."""
    try:
        stack = __REDO_QUEUE__.pop()
    except IndexError:
        LOGGER.error('No more command to redo.')
        return
    stack.redo()
    __UNDO_QUEUE__.append(stack)

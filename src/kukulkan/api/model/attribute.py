"""Attribute API for `NodeModel`."""
from blinker import Signal


class AbstractAttribute:
    """Base class for every attibute model."""
    def __init__(self, value, socket, node, name):
        self._socket = socket
        self.node = node
        self._name = name
        self.name_set = Signal()
        self.socket_set = Signal()
        self._others = set()
        self._value = value
        self.value_set = Signal()
        self.value_set.connect(self._update_other_values)

    @property
    def name(self):
        """Return the name of this `Attribute`"""
        return self._name

    @name.setter
    def name(self, new_value):
        """Set the name of this `Attribute`"""
        self._name = new_value
        self.name_set.send(new_value)

    @property
    def value(self):
        """Return the value stored on this `Attribute`."""
        return self._value

    @value.setter
    def value(self, new_value):
        """Set the value stored on this `Attribute`."""
        if self._value == new_value:
            return
        self._value = new_value
        self.value_set.send(self)

    @property
    def socket(self):
        """Return the socker type of this `Attribute`"""
        return self._socket

    @socket.setter
    def socket(self, new_value):
        """Set the socker type of this `Attribute`"""
        self._socket = new_value
        self.socket_set.send(new_value)

    def connect(self, other):
        """Connect this `Attribute` to another one.

        :param other: other attribute to drive.
        :type other: Attribute
        """
        self._others.add(other)

    def _update_other_values(self, attr):
        value = attr.value
        for other in self._others:
            other.value = value

class Attribute(AbstractAttribute):
    """An `NodeModel` attribute.

    :param value: Initial value of this `Attribute`.
    :param socket: Type of socket for this `Attribute`, either "input" or "output".
    :param editable: Whether this `Attribute` should be user editable or not.
                     By default, `True` for "input" sockets, `False` for output ones.
    :param node: Owner `Node` of this `Attribute`. This argument is meant for internal use.
    :param name: Name of this `Attribute`.
    :type socket: str
    :type editable: bool
    :type node: Node
    :type name: str
    """

    default_value = None

    def __init__(self, value=None, editable=None, socket='input', node=None, name=None):
        super().__init__(value, socket, node, name)
        self._editable = editable
        if self._editable is None:
            if socket == 'input':
                self._editable = True
            else:
                self._editable = False
        if value is None:
            value = self.default_value
        self._value = self.cast_value(value)
        self.editable_set = Signal()

    def __str__(self):
        if self.node is None or self.name is None:
            return self.__class__.__name__
        return '.'.join([str(self.node.name), self.name])

    def cast_value(self, value):
        """Convert and return an incoming value.

        This is used by the `Attribute.value` property setter method.

        :param value: New value to set on this `Attribute`.
        """
        return value

    @AbstractAttribute.value.setter
    def value(self, new_value):
        new_value = self.cast_value(new_value)
        AbstractAttribute.value.fset(self, new_value)

    @property
    def editable(self):
        """Return the editable state of this `Attribute`."""
        return self._editable

    @editable.setter
    def editable(self, new_value):
        """Set the editable state of this `Attribute`."""
        self._editable = new_value
        self.editable_set.send(new_value)

    def connect(self, other):
        """Connect this `Attribute` to another one.

        :param other: other attribute to drive.
        :type other: Attribute
        """
        if not isinstance(other, Attribute):
            raise AttributeError(
                "Can't connect a {} attribute to an {}".format(
                    self.__class__.__name__,
                    other.__class__.__name__
                )
            )
            return

        super().connect(other)
        other.value = self.value

class ExecutionAttribute(Attribute):
    """Attribute used to receive and propagate the execution flow."""
    def __init__(self, value=None, socket='input', node=None, name=None, *args, **kwargs):
        editable = False
        super().__init__(value, editable, socket, node, name)

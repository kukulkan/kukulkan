"""A model for nodes inside a kukulkan graph."""
import logging
from blinker import signal, Signal

import kukulkan.nodes
import kukulkan.attributes
from kukulkan.api.model.attribute import AbstractAttribute, ExecutionAttribute
from kukulkan.api.log import LogErrors


LOGGER = logging.getLogger(__name__)


class NodeMeta(type):
    """Auto instanciation of `Node` `Attribute` objects."""

    def __call__(cls, *args, **kwargs):
        self = type.__call__(cls, *args, **kwargs)
        self.attributes = []
        self.run = LogErrors(self.run, LOGGER)
        self.is_action_node = False
        for name, attr in cls.__dict__.items():
            if not isinstance(attr, AbstractAttribute):
                continue
            attr = attr.__class__(
                value=attr.value,
                socket=attr.socket,
                editable=attr.editable,
                node=self,
                name=name,
            )
            setattr(self, name, attr)
            self.attributes.append(attr)
            if isinstance(attr, ExecutionAttribute):
                self.is_action_node = True
            if attr.socket == 'input':
                attr.value_set.connect(self.on_attribute_changed)
        signal('node.created').send(self)
        return self


class Node(metaclass=NodeMeta):
    """A node model.

    :param name: Name to give to the node.
    :param parent: Parent `Node` of the node to create.
    :type name: str
    :type parent: Node
    """

    def __init__(self, name, parent=None):
        self._name = name
        self.parent = parent
        self._is_selected = False
        self.selection_changed = signal('selection_changed')
        self.attribute_added = Signal()
        self.deleted = Signal()
        self.name_set = Signal()

    def __str__(self):
        return self.name

    def evaluate(self):
        self.run()

    def run(self):
        """Evaluate this node."""

    def delete(self):
        self.is_selected = False
        signal('node.deleted').send(self)
        self.deleted.send()

    def add_attribute(self, name, attrtype, **kwargs):
        if hasattr(self, name):
            msg = 'Node {} already has a {} attribute.'
            raise AttributeError(msg.format(self, name))
        attr_class = kukulkan.attributes.types[attrtype]
        attr = attr_class(
            node=self,
            name=name,
            **kwargs
        )
        setattr(self, name, attr)
        self.attributes.append(attr)
        self.attribute_added.send(attr)
        if attr.socket == 'input':
            attr.value_set.connect(self.on_attribute_changed)

    def on_attribute_changed(self, attr):
        if isinstance(attr, ExecutionAttribute) and attr.value:
            self.evaluate()
        elif not any(isinstance(a, ExecutionAttribute) for a in self.attributes):
            self.evaluate()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        if self._name == new_name:
            return
        self._name = new_name
        self.name_set.send(new_name)

    @property
    def is_selected(self):
        """Property holding wether the node is selected or not."""
        return self._is_selected

    @is_selected.setter
    def is_selected(self, is_selected):
        if self._is_selected == is_selected:
            return

        self._is_selected = is_selected
        if is_selected and self not in kukulkan.nodes.selected_nodes:
            kukulkan.nodes.selected_nodes.append(self)
            self.selection_changed.send()
        elif not is_selected and self in kukulkan.nodes.selected_nodes:
            kukulkan.nodes.selected_nodes.remove(self)
            self.selection_changed.send()

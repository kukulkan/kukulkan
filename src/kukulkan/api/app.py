import abc
import os
import sys

from PyQt5 import QtCore

import kukulkan.apps
import kukulkan.events
import kukulkan.panels
import kukulkan.menus


class Application(object):
    """A wrapper around `PyQt5.QtGui.QApplication`.

    Handle application creation and event propagation.

    Also takes care of setting the right working directory.

    :attribute qapp: Underlying QApplication object.
    :attribute menus: Menus of the application.
    :attribute panels: Panels of the application.
    :type qapp: PyQt5.QtGui.QApplication
    :type menus: dict
    :type panels: dict
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        import kukulkan
        self.qapp = None
        self.menus = {}
        self.panels = {}
        os.chdir(kukulkan.__path__[0])

    def pre_build(self):
        """Create the `PyQt5.QtGui.QApplication`."""
        from PyQt5.QtWidgets import QApplication
        self.qapp = QApplication(sys.argv)
        self.qapp.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)

    @abc.abstractmethod
    def build(self):
        """Build the application content.

        This method should return a valid `PyQt5.QtGui.QWidget`.

        :rtype: PyQt5.QtGui.QWidget
        """

    def attach_menus(self, window):
        """Attach all menus to the main window.

        Menus are exposed through the ``kukulkan.extensions.menus``
        entry point.
        """
        for name, _class in kukulkan.menus.types.items():
            menu = _class(name, window)
            window.menuBar().addMenu(menu)
            self.menus[name] = menu

    def attach_panels(self, window):
        """Attach all panels to the main window.

        Panels are exposed through the ``kukulkan.extensions.panels``
        entry point.
        """
        for name, _class in kukulkan.panels.types.items():
            panel = _class(name, window)
            area = getattr(panel, 'default_area', QtCore.Qt.LeftDockWidgetArea)
            window.addDockWidget(
                area,
                panel,
            )
            self.panels[name] = panel

    def post_build(self, window):
        """Show the application window.

        Also notifies Kukulkan the application is ready
        through the ``app.ready`` event.

        Return the application exit code.
        """
        self.attach_menus(window)
        self.attach_panels(window)
        window.show()
        kukulkan.apps.current = self
        kukulkan.events.notify('app.ready')
        return self.qapp.exec_()

    def run(self):
        """Launch the application."""
        self.pre_build()
        self.window = self.build()
        sys.exit(self.post_build(self.window))

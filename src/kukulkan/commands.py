import pkg_resources
from functools import wraps

import change_case

import kukulkan.api.command


namespace = 'kukulkan.extensions.commands'


def __generate_command(cmd):

    @wraps(cmd)
    def inner(*args, **kwargs):
        with kukulkan.api.command.CommandStack():
            return cmd(*args, **kwargs)

    cmd_name = change_case.ChangeCase.pascal_to_snake(cmd.__name__)
    return cmd_name, inner


types = {}
for entry_point in pkg_resources.iter_entry_points(namespace):
    cmd_class = entry_point.load()
    types[entry_point.name] = cmd_class
    name, cmd = __generate_command(cmd_class)
    globals()[entry_point.name] = cmd_class
    globals()[name] = cmd


__all__ = types.keys()

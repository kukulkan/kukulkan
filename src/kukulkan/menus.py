import pkg_resources


namespace = 'kukulkan.extensions.menus'


types = {}
for entry_point in pkg_resources.iter_entry_points(namespace):
    types[entry_point.name] = entry_point.load()
    globals()[entry_point.name] = entry_point.load()


__all__ = types.keys()

import logging
import pkg_resources
from collections import defaultdict

from blinker import signal

namespace = 'kukulkan.extensions.nodes'


types = {}
for entry_point in pkg_resources.iter_entry_points(namespace):
    types[entry_point.name] = entry_point.load()
    globals()[entry_point.name] = entry_point.load()


all_nodes = []
selected_nodes = []
node_names = defaultdict(list)


__all__ = types.keys()


LOGGER = logging.getLogger(__name__)


def __on_node_created(model):
    """Add ``model`` to this module registries.

    :param model: Node to add to the registries.
    :type model: Node
    """
    if model not in all_nodes:
        all_nodes.append(model)
        node_names[model.name].append(model)
    else:
        LOGGER.error('Node {} already exists.'.format(model))


def __on_node_deleted(model):
    """Remove ``model`` of this module registries.

    :param model: Node to remove of the registries.
    :type model: Node
    """
    if model in all_nodes:
        all_nodes.remove(model)
        node_names[model.name].remove(model)
    else:
        LOGGER.error('Node {} already deleted.'.format(model))


def by_name(name):
    """Return all nodes with name ``name``.

    :param name: Name of the nodes to get.
    :type name: str
    :rtype: list(Node)
    """
    return node_names[name]


signal('node.created').connect(__on_node_created)
signal('node.deleted').connect(__on_node_deleted)
